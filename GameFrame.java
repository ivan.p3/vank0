import java.awt.Button;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;

public class GameFrame extends Frame {
	Map map;
	Label label;
	TextField textField;
	TextArea textArea;
	Button button;
	GameMapPanel mapPanel;
	Label label1;
	Label labelscore;
	Timer timer;
	
	GameFrame(Map m) {
		map = m;
		GameWindowListener gwl = new GameWindowListener();
		addWindowListener(gwl);
	
		
		
		/*label = new Label();
		label.setLocation(290, 50);
		label.setSize(100, 30);
		this.add(label);*/
		

		label1 = new Label();
		label1.setLocation(145, 10);
		label1.setSize(230, 230);
		label1.setText("Bob The Miner");
		label1.setFont(new Font("serif", Font.BOLD,30));
		this.add(label1);
		
		
		/*textField = new TextField();
		textField.setLocation(280, 110);
		textField.setSize(100, 30);
		this.add(textField);*/
		
		/*textArea = new TextArea();
		textArea.setLocation(280, 170);
		textArea.setSize(200, 150);
		textArea.setEnabled(false);
		this.add(textArea);*/
		
		button = new Button();
		button.setLabel("Start");
		button.setLocation(165, 250);
		button.setSize(150, 30);
		GameActionListener gal = new GameActionListener(this);
		button.addActionListener(gal);
		this.add(button);
		
		mapPanel = new GameMapPanel(map,this);
		mapPanel.setLocation(95, 50);
		mapPanel.setSize(map.sizex * 30 + 1, map.sizey * 30 + 1);
		GameKeyListener gkl = new GameKeyListener(this);
		mapPanel.addKeyListener(gkl);
		mapPanel.setVisible(false);
		
		labelscore = new Label();
		labelscore.setLocation(210, 400);
		labelscore.setSize(100, 100);
		labelscore.setFont(new Font("serif", Font.BOLD,20));
		labelscore.setText("Score " + 0);
		labelscore.setVisible(true);
		this.add(labelscore);
		
		timer = new Timer(100,new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if(mapPanel.score == 10) {
					labelscore.setText("Game over");
					mapPanel.removeKeyListener(gkl);
				}
				else {labelscore.setText("Score " + mapPanel.score);
				}
				
	
				
			}});
		timer.start();
		
		
		//GameMouseListener gml = new GameMouseListener(this);
		//mapPanel.addMouseListener(gml);
		this.add(mapPanel);
		
		this.setLayout(null);
	}

	
}
