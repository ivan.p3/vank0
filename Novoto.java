import java.awt.Frame;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Novoto {
	public static void main (String[] args) {
		
		File file = new File("toberead.txt");
		Scanner fscan;
		try {
			fscan = new Scanner(file);
		} catch (FileNotFoundException e) {
			fscan = new Scanner(System.in);
		}
		
		int mapsizex = fscan.nextInt();
		int mapsizey = fscan.nextInt();
		int heroposx = fscan.nextInt();
		int heroposy = fscan.nextInt();
		fscan.close();
		
		Hero h1 = new Hero(heroposx,heroposy);
		Map m = new Map(mapsizex,mapsizey);
		m.hero = h1;
		

		GameFrame f1 = new GameFrame(m);
		f1.map = m;
		f1.setSize(500, 500);
		f1.setVisible(true);
		
		try {
			FileWriter fw = new FileWriter("tobewritten.txt");
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		h1.setName("Bob");
		System.out.println("My name is " + h1.getName());
		System.out.println("Type start for more");
		m.print();
		Scanner scan = new Scanner(System.in);
		while (true) {
			char c = scan.next().charAt(0);
			h1.move(c,m);
			m.print();
			f1.repaint();
		}
			
	}

}
