import java.awt.Color;
import java.awt.Graphics;
import java.awt.Panel;
import java.util.Random;

public class GameMapPanel extends Panel {
	Map map;
	GameFrame frame;
	Random random1 = new Random();
	int monetkax = random1.nextInt(1, 9);
	int monetkay = random1.nextInt(1, 9);
	int bombax = random1.nextInt(1, 9);
	int bombay = random1.nextInt(1, 9);

	int score = 0;

	GameMapPanel(Map m, GameFrame f) {
		map = m;
		frame=f;
		setBounds(200, 200, 100, 100);
	}

	public void paint(Graphics g) {
		for (int y = 0; y < map.sizey; y++) {
			for (int x = 0; x < map.sizex; x++) {
				if (map.hero.posx == x && map.hero.posy == y) {
					g.setColor(Color.BLACK);
					g.fillRect(x * 30, y * 30, 30, 30);

				} else if (x == monetkax && y == monetkay) {
					g.setColor(Color.YELLOW);
					g.fillRect(x * 30, y * 30, 30, 30);

				} else if (x == bombax && y == bombay) {
					g.setColor(Color.RED);
					g.fillRect(x * 30, y * 30, 30, 30);

				}
			
				
				else {
					g.setColor(Color.BLACK);
					g.drawRect(x * 30, y * 30, 30, 30);

				}
			}
		}
		if (map.hero.posx == monetkax && map.hero.posy == monetkay) {
			monetkax = random1.nextInt(9);
			monetkay = random1.nextInt(9);
			score++;
		}
		if (map.hero.posx == bombax && map.hero.posy == bombay) {
			bombax = random1.nextInt(9);
			bombay = random1.nextInt(9);
			while(bombax == monetkax && bombay == monetkay) {
				bombax = random1.nextInt(9);
				bombay = random1.nextInt(9);
			}
			frame.labelscore.setText("You died!");
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.exit(0);
		}
	}
}

